(function ($) {

  Drupal.behaviors.jqueryui_autocomplete = {
    attach: function (context, settings) {
      $('input[name="title"]').autocomplete({
        source: Drupal.settings.basePath + 'test/autocomplete'
      });
    }
  };

}(jQuery));
